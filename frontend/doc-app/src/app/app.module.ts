import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {GetDocumentComponent} from './get-document/get-document.component';
import {PutDocumentComponent} from './put-document/put-document.component';
import {DocumentComponent} from './document/document.component';

@NgModule({
  declarations: [
    AppComponent,
    GetDocumentComponent,
    PutDocumentComponent,
    DocumentComponent
  ],
  imports: [
    BrowserModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
