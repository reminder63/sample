import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PutDocumentComponent } from './put-document.component';

describe('PutDocumentComponent', () => {
  let component: PutDocumentComponent;
  let fixture: ComponentFixture<PutDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PutDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PutDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
