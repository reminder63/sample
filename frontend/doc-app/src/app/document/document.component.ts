import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DocumentModel} from "../document.model";

@Component({
  selector: 'dc-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.less']
})
export class DocumentComponent implements OnInit {
  @Input()
  isEditable: boolean;

  @Input()
  document: DocumentModel;

  @Output()
  putButtonEmit = new EventEmitter<DocumentModel>();

  documentDate: Date;
  documentId: number;
  documentName: string;

  constructor() {
  }

  ngOnInit() {
    this.documentName = this.document.name;
    this.documentDate = this.document.date;
    this.documentId = this.document.id;
  }

  putDocument() {
    this.putButtonEmit.emit({
      id: this.documentId,
      name  : this.documentName,
      date: this.documentDate
    });
  }

}
