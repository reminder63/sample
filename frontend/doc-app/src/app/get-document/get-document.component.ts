import {Component, OnInit} from '@angular/core';
import {DocumentModel} from "../document.model";
import {HttpClient} from "@angular/common/http";
import {take} from "rxjs/operators";
import {environment} from "../../environments/environment";

@Component({
  selector: 'dc-get-document',
  templateUrl: './get-document.component.html',
  styleUrls: ['./get-document.component.less']
})
export class GetDocumentComponent implements OnInit {
  document: DocumentModel;
  documentId: number;
  isResultsReady: boolean;

  constructor(private httpService: HttpClient) {
  }

  ngOnInit() {
  }

  getDocument() {
    const url = environment.url + "/document/" + this.documentId;
    this.isResultsReady = false;
    this.httpService.get(url).pipe(take(1)).subscribe((document: DocumentModel) => {
      console.log(document);
      this.document = document;
      this.isResultsReady = true;
    });
  }
}
