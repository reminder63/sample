package com.netcracker.example.sample.controller;

import com.netcracker.example.sample.domain.Document;
import com.netcracker.example.sample.service.DocumentService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/document")
public class DocumentController {

    private DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @PutMapping()
    public Document createDocument(@RequestBody Document document) {
        return documentService.createDocument(document);
    }

    @GetMapping(path = "/{id}")
    public Document findById(@PathVariable("id") Long id) throws Exception{
        return documentService.findById(id);
    }
}
