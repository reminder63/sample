package com.netcracker.example.sample.repository;

import com.netcracker.example.sample.domain.Document;
import org.springframework.data.repository.CrudRepository;

public interface DocumentRepository extends CrudRepository<Document, Long> {
}
